#include <msp430.h> 
#include <stdint.h>

/* Functieprototypen voor de meegeleverde "oled_lib" */

/* Deze functie configureert het display
 * via i2c en tekent alvast het kader.
 */
void initDisplay();

/* Deze functie past de weer te geven
 * temperatuur aan.
 *
 * int temp: De weer te geven temperatuur.
 * Max is 999, min is -99.
 * Hierbuiten wordt "Err-" weergegeven.
 *
 * Voorbeeld:
 * setTemp(100); // geef 10.0 graden weer
 */
void setTemp(int temp);

/* Deze functie geeft bovenaan in het
 * display een titel weer.
 * char tekst[]: de weer te geven tekst
 *
 * Voorbeeld:
 * setTitle("Hallo"); // geef Hallo weer
 */
void setTitle(char tekst[]);

int ADC = 0;
static int temp = 0;

int ADCtoTemp(int ADC)
{
    return ((ADC * 1500) / 1023);
}

int main(void)
{
    WDTCTL = WDTPW | WDTHOLD; // Stop watchdog timer

    // Stel klok in op 16MHz
    DCOCTL = 0;
    BCSCTL1 = CALBC1_16MHZ;
    DCOCTL = CALDCO_16MHZ;

    initDisplay();
    setTitle("Opdracht7.1.13");

    ADC10CTL1 = 0;
    ADC10CTL1 |= INCH_5 | SHS_0 | ADC10DIV_7 | ADC10SSEL_0 | CONSEQ_0;
    ADC10CTL1 &= ~(ADC10DF | ISSH);

    ADC10CTL0 = 0;
    ADC10CTL0 |= SREF0 | ADC10SHT0 | ADC10SR | REFBURST | ENC | REFON | ADC10ON
            | ADC10IE;
    ADC10CTL0 &=
            ~(SREF2 | SREF1 | ADC10SHT1 | REFOUT | MSC | REF2_5V | ADC10SC);

    __enable_interrupt();

    while (1)
    {
        // Hoog fictieve temperatuur op met 0.1 graad
        // Geef dit weer op het display
        setTemp(temp);
        // Niet te snel...
        __delay_cycles(8000000);
        ADC10CTL0 |= ADC10SC;
    }
}

#pragma vector = ADC10_VECTOR

__interrupt void TempuratureConversion(void)
{
    temp = ADCtoTemp(ADC10MEM);
    ADC10CTL0 &= ~(ADC10IFG);
}
